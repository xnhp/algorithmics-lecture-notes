\documentclass[10pt]{article}



\usepackage[utf8]{inputenc}

\usepackage{zsfgv}
\usepackage{multicol}
\usepackage{etoc}

\sloppy
\begin{document}
\begin{multicols}{2}

\pagebreak
\part{Network Flows \& Matchings}
\localtableofcontents


\section{Flows}
\subsection{Basics}

\paragraph{\ild{$s-t$-flow}} is a function $f$ that satisfies
\begin{itemize}
\item \ild{capacity}: $\forall e \in E: 0 \leq f(e) \leq c(e)$ \\
  \note{where $c$ denotes edge capacity (static)}
\item \ild{conservation}: $\forall v \in V - \{s,t\}:$ \\
  \begin{align*}
    \sum_{\outof{v}{e}} f(e) = \sum_{\into{v}{e}} f(e)
  \end{align*}
\end{itemize}
\note{For each node, in-flow equals out-flow}

  \note{An often-used argument is }
\textcolor{olive}{
    \begin{align*}
      \sum_{\outof{v}{e}} f(e) - \sum_{\into{v}{e}} f(e) = 0
    \end{align*}
  }

\paragraph{\ild{value}} of a flow $f$:
\begin{align*}
  \ild{v}(f) := \sum_{\outof{s}{e}} f(e)
\end{align*}

\paragraph{\ild{$s-t$-Cut}} is a partition $(A,B)$ of $V$ with $s \in A$ and
$t \in B$.

\paragraph{\ild{Capacity} of a cut}
\begin{align*}
  \ild{cap}(A,B) := \sum_{\outof{A}{e}} c(e)
\end{align*}

\paragraph{\thm{Flow value lemma}} Let $f$ be a flow, let $(A,B)$ be any cut.
Then
\begin{align*}
  v(f) = \sum_{\outof{A}{e}} f(e) ~-~ \sum_{\into{A}{e}} f(e)
\end{align*}
\proof{
  • Apply definition • expand to sum over $A$ (since $s \in A$
  other terms are $0$) • use conservation property • case distinction (what kind
  of vertex/edge combinations will we have in inner sum?). Really mostly just
  writing the sum in a new way, no real ideas.
}

\paragraph{\thm{Weak Duality}} Let $f$ be any flow. Then, for any cut, we have
\begin{align*}
  v(f) \leq cap(A,B)
\end{align*}
\proof{
  • flow value lemma  • inequality towards definition
}

\paragraph{\ild{Certificate of Optimality}} If for a flow $f$, $v(f) =
cap(A,B)$, then
\begin{itemize}
\item $f$ is maximal (maximum flow)
\item $(A,B)$ is minimal (minumum cut)
\end{itemize}
\proof{ • If equality holds, then by weak duality any other flow is
  smaller-equal, thus $f$ is maximal • vice versa for cuts. }

\paragraph{\ild{Augmenting Path}} is a directed path $P$ from $s$ to $t$ so that
$\forall e \in P: f(e) < c(e)$. Its \ild{bottleneck value} is $b_P := \min_{e \in
  P}\{c(e)-f(e)\}$. Then, we can find a flow $f'$ defined by $f'(e) = f(e) +
b_P$ (\ild{augmenting} $f$ by $P$).

\paragraph{\ild{Residual Graph}} Replace each edge with assigned flow and
capacity by an arc in either direction ($e$ and $e^R$).
\begin{itemize}
\item Forward-arc carries \textit{leftover capacity}: $c_f(e) := c(e) - f(e) $
\item Backward-arc carries \textit{undo capacity}: $c_f(e^R) := f(e)$.
\item \note{(only consider \textit{nonempty}, \textit{positive} arcs --
    potential arcs with capacity 0 are not included!)}
\end{itemize}

\paragraph{\ila{\textsc{Ford-Fulkerson} Algorithm}}
Procedure:
\begin{enumerate}
\item Construct residual graph
\item Find augmenting path and augment flow along this path
\item Repeat until no further augmenting path
\end{enumerate}
Considerations:
\begin{itemize}
\item Assume integer, capacities bounded by $C$
\item Algorithm runs in $\mathcal{O}(mnC)$ time: Augm. path can be found by DFS
  in $\mathcal{O}(n+m)$ time, here: $\mathcal{O}(m)$ because no isolated
  vertices. Since each iteration increases the value of the constructed flow by
  at least $1$, the number of iterations is bounded by the value of the flow. An
  upper bound for this is $nC$ (consider sum).
\end{itemize}


\paragraph{\thm{Max-flow/Min-cut Theorem}} The value of a maximal flow is equal
to the capacity of a minimal cut.
\proof{
  We need to show that the following are equivalent:
  \begin{enumerate}
  \item There exists a cut $(A,B)$ with $v(f) = cap(A,B)$.
  \item $f$ is maximal
  \item $G_f$ has no augmenting path
  \end{enumerate}
  For $(3) \Rightarrow (1)$, consider nodes reachable from $s$ as the set $A$
  inducing the cut we are looking for (that is, \textsc{Ford-Fulkerson} also
  finds us this min-cut).
}

\paragraph{\ild{Capacity Scaling}} Begin with a \textit{scaling parameter}
$\Delta$, when looking for an augmenting path consider only these with
bottleneck value $b_P \geq \Delta$, if none found decrease $\Delta$ and try again.

\textcolor{gray}{Details, correctness and complexity omitted. Maybe later.}

\subsection{Circulations with Demands}

\paragraph{\ild{Circulation}} is a function $f$ that satisfies
\begin{itemize}
\item \textit{capacity}: $\forall e \in E: 0 \leq f(e) \leq c(e)$
\item \textit{conservation}: $\forall v \in V: \sum_{\into{v}{e}} f(e) -
  \sum_{\outof{v}{e}} f(e) = d(v)$ \note{i.e. $d(v)$ units ``leave'' the network
  at $v$, hence we need that much in-flow.}
\end{itemize}
We say:
\begin{itemize}
\item \ild{demand node}: $d(v) > 0$
\end{itemize}

\paragraph{\thm{Lemma}} There exists a circulation $\Rightarrow$ sum of supplies ($D$)
equals sum of demands. \proof{Transform into $\sum_{v}d(v)$, then apply
  conservation}

\paragraph{Max flow formulation}
\begin{itemize}
\item Add additional source, sink node
\item For each supply node (negative demand), add arc from source node with
  $d(v)$ as capacity
\item For each demand node, add arc to target node with $d(v)$ as capacity.
\end{itemize}

\paragraph{\thm{Thm}} $G$ has a circulation $\Leftrightarrow$ $G'$ has a \textit{maximal}  flow of
value $D$.

\subsection{Circulations with Demands and Lower Bounds}

\paragraph{\ild{Circulation with lower Bounds}} is a function that satisfies
\begin{itemize}
\item \textit{capacity} respecting lower bounds: \\ $\forall e \in E: l(e) \leq
  f(e) \leq c(e)$
\item \textit{conservation} respecting demands: \\ $\sum_{\outof{v}{e}}f(e) -
  \sum_{\into{v}{e}} f(e) = d(v)$
\end{itemize}

\paragraph{\thm{Thm}} There exists a circulation in $G$ $\Leftrightarrow$ there exists
a circulation in $G'$.
\proof{Model lower bounds with demands. Add lower bound to source node, remove
  it from target node. New capacity is upper bound minus lower bound.
  \note{$l$ units leave the network at source node and reenter it at target node}}.

\subsection{Integrality}

...

\subsection{Exercises}

\begin{itemize}
\item Describe the Feasible Matrix Rounding problem and how it can be modelled
  and solved by a circulation with lower bounds
\item Circulations with lower bounds: There exists a circulation in $G$
  $\Leftrightarrow$ there exists a circulation in $G'$
\item Show that, given a partition $(A,B)$ such that $A$ contains all demand
  nodes and $D > cap(A,B)$ (where $D$ is sum of supplies/demands), then there
  exists no circulation.
\end{itemize}

Notes on exercises

{\small
  \paragraph{Ex. 2} Really only need to consider residual graph and the cut
  induced by $f$. If edge modification creates a new forward arc across that cut,
  only then we have an augmenting path and a new, increased maximal flow $f'$. \par
}

{\small
\paragraph{Ex. 4} Key idea here are ``loops'' (cycles) attached to an augmenting
path, can then either take only that path or that path with the loop as a flow.
Path \textit{with loop} can be constructed to be critical but path without loop
is also a max flow since value would be bounded by capacities of nodes on path
outside of loop.
}


\section{Bipartite Matchings}

\begin{itemize}
\item $M \subseteq E$ is a \ild{matching} iff each vertex appears in at most one
  edge of $M$ ($v$ is \ild{$M$}-covered or \ild{$M$-exposed})
\item A matching is \ild{perfect} if $2 ||M|| = ||V||$
\end{itemize}


Objective: Find a maximum cardinality matching in a bipartite graph.

\paragraph{\thm{Max flow formulation}} Add arcs from new source node to all
nodes in $A$, from all nodes in $B$ add arcs to target node (all with unit
capacity). Then, the size of a maximum cardinality matching in $G$ is the value
of a maximum flow in $G'$.
\proof{}
\begin{itemize}
\item[$\Rightarrow$] $M$ defines a flow that is maximal
\item[$\Leftarrow$] By integrality theorem, there exists an integral flow, by
  constraints it is binary. 
\end{itemize}


\paragraph{\ild{One-sided canonical decomposition}} Let $(V = V_1 \cup V_2, E)$
be a biparite graph and $M$ a \textit{maximum} matching. \begin{itemize}
\item Let edges in $M$ now be directed $V_1 \rightarrow V_2$, other edges $V_2
  \rightarrow V_1$.
\item Let $R, G \subset V$ be defined as
\begin{itemize}
\item $R$: Vertices that can be reached from from any $M$-exposed vertex in $V_2$.
\item $G$: Remaining vertices
\end{itemize}
\end{itemize}

\paragraph{\thm{Thm}} The one-sided canonical decomposition of a maximum
matching is unique.
\proof{Maybe later.}

\paragraph{\thm{König-Egerváry}} $(V_1 \cap R) \cup (V_2 \cap G)$ is a minimum
vertex cover. Thus, the size of a smallest vertex cover of a bipartite graph
equals the size of a maximum matching.

\begin{itemize}
\item \textsc{VC} on general graphs is NP-hard
\item Since maximum matchings can be found in polytime, a minimal \textsc{VC} on
  \textit{bipartite graphs} can also be found in polytime (via the one-sided
  canonical decomposition)
\end{itemize}

\paragraph{\thm{Marriage Theorem}} Let $G$ be a bipartite graph with $||V_1|| =
||V_2||$. Then, $G$ has a perfect matching iff $||N(S)|| \geq ||S||$ for all $S
\subseteq V_1$.
\proof{
 ($\Rightarrow$)~ assume ex. no perfect matching; consider decomp. of a maximum
 matching (must contain exposed vertices), take $S = G \cap V_1$
}

\subsection{Edge-Disjoint Paths}

\textcolor{gray}{Maybe later.}

\subsection{Exercises}
\begin{itemize}
\item Illustrate a one-sided red-green decomposition, its relation to a maximal
  matching and a minimal vertex cover.
\end{itemize}

\part{Planarity, A* Search, and Randomized Algorithms}
\localtableofcontents

\section{Planar Graphs}

\paragraph{\thm{Euler's formula}} For any planar embedding, we have
\begin{align*}
  n - m + f = 2
\end{align*}
\proof{By induction over $m$}

\paragraph{\thm{Cor. for planar graphs}} For a simple, planar graph with $n \geq
3$, we have
\begin{align*}
  m \leq 3n - 6
\end{align*}
\proof{With handshaking lemma: $f \leq \nicefrac{2}{3}m$, then plug in.}

\paragraph{\thm{Cor. for planar, bipartite graphs}} For any simple, planar,
\textit{bipartite} graph with $n \geq 3$, we have
\begin{align*}
  m \leq 2n - 4
\end{align*}
\proof{Exercise 1 from sheet 2. A bipartite graph has only cycles of even length
$\geq$ 4. So, every face is bounded by at least 4 half-edges, of which each two
correspond to one edge, so $4f \leq \sum_{f \in F} deg(f) = 2m$; plug into
eulers formula. }

\paragraph{\thm{Vert. of deg 5}} A simple, planar graph contains a vertex of
degree at most $5$.

\paragraph{\thm{Kuratowski}} A graph is planar iff it does not contain a
subdivision of $K_5$ or $K_{3,3}$.

\paragraph{\thm{Wagner}} A graph is planar iff it has no minor isomorphic to
$k_5$ or $K_{3,3}$.

\paragraph{\thm{Cor.}} Planarity can be tested in linear time.

\section{A*-Search}
\paragraph{\ila{Algorithm}}
\begin{itemize}
\item Similar to BFS/Dijkstra (Dijkstra is A* with $h=0$)
\item Put node with smallest value for \textit{tentative distance} +
  \textit{heuristic} into queue for future expansion.
\item For new tentative distances, do \textit{not} consider heuristics values
  added when putting something in the queue. ($Q$ and $g$ are different things).
\end{itemize}

\paragraph{Note:}
\begin{itemize}
\item A node may be expanded more than once \note{Because we additionally add the
  heuristic value before putting a node in the queue, this is unlike Dijkstra
  where (because of that and the min-queue) we can't be sure that a node does not
  have to be expanded more than once.}
\item Thus, A* has to continue until target node is selected for expansion.
\end{itemize}

\paragraph{Termination} only considers the finitely many acyclic paths (cycles would increase cost)

\paragraph{\ild{Admissible heuristic}} Let $h^*(x)$ be the optimal cost from $x$
to $t$. A heuristic $h$ is \ild{admissible} iff it is a lower bound, i.e.
\begin{align*}
  h(x) \leq h^*(x)
\end{align*}
for all $x \in V$. \note{i.e. if it does not overestimate the remaining distance
to $t$.}

\paragraph{\thm{Correctness}} If $h$ is admissible, A* will always find a shortest path.
\proof{
  Assume a non-optimal path $p'$ with cost $f'$ was found. Then there ex. an unexpanded (because
  else it would be on $p'$ and $p'$ would be optimal) node $x$ on 
  the shortest path, with $f(x) = g(x) + h(x) \geq f'$ (else it would have been
  expanded by A*). Then $f(x) = g(x) + h(x) = g*(x) + h(x) \leq g*(x) + h*(x) =
  f*$, contradiction.
}

\paragraph{\ild{Monotonic heuristic}} $h$ is \ild{monotonic} iff
\begin{align*}
  \forall (x,y) \in E: ~~ h(x) \leq w_{xy} + h(y) ~~~ \text{and} ~~~ h(t)=0
\end{align*}
\note{i.e. impossible to decrease $f(x)$ by extending a path to include a
  neighbouring node.}

\paragraph{\thm{No re-expansion}} If $h$ is monotonic, A* expands no node twice.
\proof{Consider edge $x \rightarrow y$, use def. of $f(x)$ and $f(y)$ and
  monotonicity to show that $f(x) \leq f(y)$. A* always extends node with
  smallest $f$-value next. If monotonicity is given and we have already expanded
  a node, we know that any other path to this node has to have a higher
  $f$-value.}

\paragraph{\thm{Thm}} $h$ monotonic $\Rightarrow$ $h$ admissible

\proof{
  Consider a shortest path $(s,x,y, ..., t)$. Since $h$ is monotonic, we have
  $$
  h(s) \leq h(x) + w_{xy} \leq h(y) + w_{xy} + w_{sx} \leq ... \leq h(t) +
  w_{tz} + ... + w_{xy} + w_{sx} = h^*(x)
  $$
}


\vfill
\pagebreak
\section{Randomized Algorithms}
\paragraph{\ild{Monte Carlo Algorithm}} guaranteed runtime but uncertain output.
\note{(When you go to Monte Carlo, you can only stay for so long until you've
  bet all your money. The outcome, however, is uncertain.)}
\paragraph{\ild{Las Vegas Algorithm}} guaranteed correct output but uncertain
runtime. \note{(When you go to Las Vegas, youre sure to get married, its just a
  matter of time)}
\paragraph{\iln{Miller-Rabin Primality Test}} For $s$ iterations, check if a
random number is witness for $n$ being not prime. Example for a \textit{Monte
  Carlo algorithm}.

\subsection{Basics of Probability Theory}

\paragraph{\ild{Sample Space, Event}}.

\paragraph{\ild{Conditional Probability}} of $E$, given that $F$ has occured:
\begin{align*}
  \cPr{E}{F} = \frac{P(E \cap F)}{P(F)}
\end{align*}

\paragraph{\ild{Independence}} iff
\begin{align*}
  P( \bigcap_{i \in I} E_i) = \prod_{i \in I} P(E_i)
\end{align*}

\paragraph{\thm{Union Bound}} It holds that
\begin{align*}
  P( \bigcup E_i) \leq \sum P(E_i)
\end{align*}

\paragraph{\ild{Random Variable}} $X : \Omega \rightarrow \mathbb{N}$ where
$X=j$ is an event.

\paragraph{\ild{Expected Value} of X} $E(X) = \sum_{j=0}^{\infty} j \cdot P(X=j)$

\paragraph{\thm{Linearity of Expectation}} For random variables $X$, $Y$.
\begin{align*}
  E(X+Y) = E(X) + E(Y)
\end{align*}

\paragraph{\thm{Lemma}} We have
\begin{align*}
  \sum^n \nicefrac{1}{n-i} = \sum^n \nicefrac{1}{i} = H_n \approx \ln n
\end{align*}


\paragraph{\iln{Waiting Time Bound}} Let success probability be $p$, so prob. of
failure is $1-p$. Let $X$ (random var) be the number of (independent) trials
until first success. Then:
\begin{align*}
  E(X) = \sum_{j=0}^{\infty} j (1-p)^{j-1} p = ... = \nicefrac{1}{p}
\end{align*}
\proof{maybe later, probably irrelevant}


\paragraph{\iln{Number of correct guesses}} Let $X$ be number of correct guesses
(e.g. top card from a deck). Let $X_i$ be 1 iff $i$-th card was guessed
correctly. Then $X = \sum X_i$.
\begin{itemize}
\item \textit{Memoryless}
  \begin{itemize}
  \item $E(X_i) = 0 \cdot P(X_i=0) + 1 \cdot P(X_i=1) = P(X_i=1) = \nicefrac{1}{n}$
  \item $E(X) = n \cdot \nicefrac{1}{n}$.
  \end{itemize}
\item \textit{With memory}
  \begin{itemize}
  \item $E(X_i) = P(X_i=1) = \frac{1}{n-i+1}$ \note{wont pick previously picked
      card again, thus reduced sample space}
  \item $E(X) = \sum E(X_i) = \sum
    \nicefrac{1}{i} = H_n \approx \log n$
  \end{itemize}
\end{itemize}

\paragraph{\thm{Lemma}} To emphasize: For binary random variables, we have
\begin{align*}
  E(X) = 0 \cdot E(X=0) + 1 \cdot E(X=1) = P(X=1)
\end{align*}


\paragraph{\iln{Coupon Collector's Problem}} Want to collect $n$ types of
coupons, each is equally likely to be drawn. What is the expected number of
rounds until I have all types of coupons?
\begin{itemize}
\item Let $X$ be number of rounds, looking for $E(X)$.
\item Probability to obtain $(j+1)$-th coupon $p(j)$: $\nicefrac{n-j}{n}$
\item Let $X_j$ be the number of rounds until $(j+1)$-th coupon is obtained.
  Then $X = \sum X_i$.
\item  So,
  \begin{align*}
    E(X_j) = \nicefrac{1}{p(j)} = \nicefrac{n}{n-j} = n H_n
  \end{align*}
  by waiting time bound
\item And thus
  \begin{align*}
    E(X) = \sum E(X_i) = n \sum \nicefrac{1}{n-j} = n H_n \approx n \ln n
  \end{align*}
\end{itemize}


\paragraph{\iln{QuickSort}} Want to find out expected number of comparisons
$E(C)$. Define binary random variable $X_{ij}$ denoting whether input elements
$x_i$ and $x_j$ are compared. $x_i$ and $x_j$ are compared iff one of them is
pivot. This is the case iff one of either is the \textit{lca} of their common
division subtree. Candidates for the \textit{lca} are $x_i, ..., x_j$, all other
elements certainly not because we have a BST. So, $P(X_{ij}) = \frac{2}{j-i+1}$.
Then plug into $E(C)$.

\paragraph{\iln{Contention Resolution}} \textcolor{gray}{Maybe later.}





\vfill
\pagebreak
\part{Structural Decompositions and Algorithms}

\section{Tree Decompositions}
\paragraph{\ild{Tree decomposition}} A $\sim$ is a pair $(T, \chi)$ with
$T=(V',E')$ tree and $\chi: V' \rightarrow P(V)$ (\ild{bags}) with
\begin{itemize}
\item $\cup_{v \in V_t} \chi(v) = V_G$ -- \iln{All nodes are contained in some bag}
\item $\forall (v,w) \in E: \exists t \in V_t: v,w \in \chi(t)$ -- \iln{If there
  is an edge between two nodes, they appear together in \textit{some} bag.}
\item For all nodes $v \in V_G$, the induced subgraph of all bags containing $v$
  is a \textit{connected} subtree.
\end{itemize}
The \ild{width} of a tree decomposition is the size of its largest bag minus one.

\paragraph{\thm{Properties}}
\begin{itemize}
\item If $H$ is a subgraph of $G$ then $\tw H \leq \tw G$
\item If $G$ is disjoint union of $G_1$ and $G_2$ then $\tw G = \max \{ \tw G_1,
  \tw G_2\}$
\item If $G$ has min. degree $d$, then $d \leq \tw G$ \proof{ Assume tree
    decomp. is small. In particular, for a pair $t, t'$ of adjacent tree nodes,
    $\chi(t) \not \subseteq \chi(t')$. Then there ex. a vertex $v \in \chi(t) -
    \chi(t')$. By connectivity (T3), $v$ can be in no other bag preceeding $t'$
    (argue similarly in the other direction if $t$ is not a leaf.) Thus, all
    neighbours of $v$ must be $\chi(t)$.  }
\end{itemize}

\paragraph{\iln{Variable Elimination for SAT}} Maybe later.

\paragraph{\ild{Small Tree Decomposition}} A tree decomp. is \ild{small} if
there is no pair $t, t'$ of distinct tree nodes so that $\chi(t) \subseteq
\chi(t')$.

\paragraph{\ild{Nice tree decomposition}} A tree decomp is \ild{nice} iff it
consists only of  • leaf  • introduce and  • forget nodes.
\paragraph{\thm{Thm}} A nice tree decomp. can be computed in time $f(k) p(n)$.

\paragraph{\ild{Existence of small tree decomp}} There is a polytime algorithm
that turns any tree decomp. into a small tree decomp.

\paragraph{\thm{Lower bound by Minor}} Let $H$ be a minor of $G$. Then
\begin{align*}
  \tw H \leq \tw G
\end{align*}
\iln{Can check minor property in time $f(H) n_G^3$}

\paragraph{\iln{Examples}}
\begin{itemize}
\item complete graph $K_n$ (clique) has treewidth $n-1$ ($n$ nodes in largest bag).
\end{itemize}


\iln{Graph classes of some excluded minor are minor-closed. Hence, we can talk
  about the treewidth of classes characterised by some excluded minors.}

\paragraph{\thm{Robertson/Seymour}} A graph class $C$ has bounded treewidth iff
there is a $k$ such that the $k$-grid graph is not a minor of any graph in $C$.

\paragraph{\thm{Bags are separators}} ... \note{Basis for the approach to use a
  tree decomp for dynamic programming.}

\paragraph{\iln{Notes on DynProg on tree decomps}}
\begin{itemize}
\item \textbf{Introduce node} $t$ has by definition only one child $t'$.
  Introduced node $v$ does not have any edges to a node strictly below $t'$,
  else $v$ would have appeared earlier.
\item \textbf{Join node} $t$ connecting $t'$ and $t''$ -- have no edges between
  vertices in subtrees rooted by $t'$ and $t''$ (else they would be joined earlier.)
\end{itemize}


\section{Courcelle's Theorem}
\paragraph{\textit{Background}} A Graph $G=(V,E)$ can be seen as a $E$-structure
on the domain $V$, that is: $E$ is a relation between nodes. \textit{First-order
  logic} allows boolean logical operations as well as aforementioned relations.
\ild{Monadic Second-order logic} (MSO) additonally allows set variables and
quantification over sets (so, predicates with a single argument (\textit{monadic
predicates}), those define set membership), (but not all predicates). In the
simple case, we have only vertices as objects ($V$ as domain) but could also
consider all vertices and edges and then use predicates to distinguish (the
corresponding graph has same treewidth.)

\paragraph{\thm{Courcelle's Theorem}} Let $A$ be an $n$-element structure and
let $\varphi$ be a MSO formula. Then there exists an algorithm which, given a
tree decomp of width $k$ determines whether $A \models \varphi$ in time
$f(|\varphi|, k) \cdot n + \mathcal{O}(||A||)$. So, this can be done in FPT time
parametrized by the treewidth of $A$ and $|\varphi|$.

\paragraph{\iln{Note}} For simple $E$-structures, the graph in question here is
simply the graph itself ($\rightarrow$ \ild{Gaifman Graph})

\paragraph{\iln{Note}} Consequently, when designing such formulas, one has to
watch out that their size must not be dependent on $n$, else FPT time is not
given anymore.

\paragraph{\iln{Exercise}} \textit{Given a tree decomp. of width $d$, show how
  to obtain an elimination ordering of width at most $d$}

Let $t$ be a leaf in the tree decomp, let $t'$ be its parent node. Let $v \in
\chi(t)$ be a vertex in the leaf bag. --- We know that $\chi(t) \cap \chi(t')$ is a
separator in $G$ of the vertices contained in the subtrees $T$, $T'$ of the tree
decomp obtained by removing the edge $(t,t')$ \note{(see Ex.4)}. --- Since $t$ is a
leaf bag, its subtree is only that leaf bag, i.e. $T = \chi(t)$. Since $\chi(t)
\cap \chi(t')$ is a separator, any neighbour of $v$ is in $T=\chi(t)$ or in
$(\chi(t) \cap \chi(t'))$ , i.e. is in $\chi(t) \cup (\chi(t) \cap \chi(t'))$
which is smaller than $d+1$, so $v$ can have at most $d$ neighbours. --- So, the
strategy is to do a postoder traversal over the tree decomp and successively
remove the vertices that are in the current leaf bag in any order.



\vfill
\pagebreak


\part{Parameterized Algorithms}
\paragraph{\iln{Note}} \textcolor{gray}{I very much recommend the material from the course
  \textit{Fixed-Parameter Algorithms and Complexity} for additional exercises.}

\section{Definitions}

\paragraph{\ild{Def}} A \ild{Fixed-Parameter Algorithm} for a parametrize
problem is an algorithm tht run in $f(k) \cdot p(n)$ time where $f$ is an
arbitrary computable function dependent only on $k$ and $p$ is a polynomial
function independent of $k$.

\paragraph{\ild{Def}} Let $(I,k)$  be an instance of a parametrized problem. A
\ild{kernelization algorithm} is a \textit{polynomial-time} algorithm which maps
$(I,k) \mapsto (I', k')$ s.t.
\begin{itemize}
\item $(I,k)$ is a yes-instance $\Leftrightarrow$ $(I',k')$ is a yes-instance
\item There ex. a computable function $h$ s.t. $|I'| \leq h(k)$ and $k' \leq
  h(k)$.
\end{itemize}
Then, $(I', k')$ is the \ild{kernel}. If $h \in \mathit{poly}(k)$, then it is a
\ild{polynomial kernel}.

\paragraph{\thm{Thm}} $\mathcal{P}$ is FPT $\Leftrightarrow$ $\mathcal{P}$
admits a kernelisation. \proof{($\Rightarrow$) Case $f(k) \geq n$: Then instance
  is a kernel to itself (check definition); Case $f(k) < n$: Can use algorithm
  to compute solution and return a corresponding trivial, constant-sized
  instance (as a kernel) within time bounds required for kernelisation.
  ($\Leftarrow$) $\mathcal{P}$ is computable, so there exists an algorithm.
  Executing it on the kernel, its runtime is dependent only on the small size of
the kernel, i.e. on $h(k)$ so that's fine.}


\paragraph{\iln{Note}}
\begin{itemize}
\item Every FPT problem has a (trivial) kernel.
\item Not all FPT problems have \textit{polynomial} kernels.
\end{itemize}


\paragraph{\iln{A strategy for proving kernels}} Come up with reduction rules to
reduce the instance and potentially also $k$. Argue, that, after exhaustively
applying these rules, the new instance is either too large (can output small,
trivial no-instance) or is sufficiently small to qualify as a kernel, or can
immediately deduce that it must be a yes-instance.

\paragraph{\iln{Kernel for Vertex Cover}} We came up with reduction rules but didn't
really say what the size of the obtained kernel after no more rules can be
applied. Instead, we just looked at the situation and saw that if the obtained
kernel is ``too large'' then it could never contain a valid solution (always is
a no-instance, can output trivial no-instance of arbitrarily small size, this
always is polynomial kernel). --- Inequality in second case follows from
handshake lemma.

\paragraph{\iln{Kernel for Hitting Set (Sunflower Lemma)}} In the latter case,
we indeed have a polykernel because reduction through the sunflower reduces
instance size by $k$. Since a sunflower can be found in polytime, I can repeat
this rule as often as I want (will still be polytime), reducing $k$ until it is
in $\mathcal{O}(k^d)$.

\section{Exercises/Examples}
Stuff you \textit{have to} be able to do. We assume all problems to be parametrized
\begin{itemize}
\item \textit{Show that \textsc{Independent Set on Bounded Degree}} is FPT
  {\footnotesize Same approach as for VC}
\item \textit{Show that \textsc{Parametrized Vertex Cover} is FPT {\footnotesize Bounded
  search tree approach, make local observation and derive bound for branching factor.}}
\item \textit{Show that if a problem admits a kernelization it is FPT.}
\item \textit{Show that if a problem is FPT, it admits a kernelization.}
\item \textit{Show that \textsc{Parametrized Vertex Cover} admits a polynomial kernel. } {\footnotesize Come up with reduction rules, show their safety and polyn. runtime, show that we can obtain polynomial kernel after no more rules can be applied (case distinction)}
\end{itemize}



\vfill
\pagebreak
\part{Mathematical Programming}

\paragraph{\ild{Linear Programming}} is the problem of minimizing a
\textit{linear} cost function subject to (in)equality constraints that are
linear.

\paragraph{\ild{General form}}
\begin{align*}
  \text{min} ~~&~~ c^T x \\
  \text{s.t.} ~~&~~ Ax \geq b \\
  ~~&~~ x \in \mathbb{R}^n
\end{align*}
(Other inequalities can be transformed into this general form)

\paragraph{\ild{Standard form}}
\begin{align*}
  \text{min} ~~&~~ c^T x \\
  \text{s.t.} ~~&~~ Ax = b \\
  ~~&~~ x \geq 0
\end{align*}
(General form can be translated into standard form -- slack/surplus variables,
$x_j^+ - x_j^i$ for unrestricted (unbounded) variables.)


\textcolor{gray}{skipping theory and proof on existence of optimal solution}

\paragraph{\thm{Duality gap}} todo...

\paragraph{\iln{Discrete Alternatives or Disjunctions}}
If exactly one of $a_1 x \leq b_1$ or $a_2 x \leq b_2$ should be fulfilled, we
can introduce binary variables $y_1$ and $y_2$ and constraints
\begin{align*}
  y_1 + y_2 & = 1 \\
  a_i x - b_i & \leq M(1-y_i) ~~~ \text{for $i \in \{1,2\}$}
\end{align*}
Then, if $y_i = 1$, rhs of corresponding inequality is 0, so constraint has to
be fulfilled exactly. If $y_i = 0$, rhs of corresponding inequality is $M$, if
we chose an upper bound this is effectively unconstrained.

\paragraph{\iln{Linking Variables}} If we have a decision variable $y_t \in \{0,1\}$ concerning a
modeled object and another variable $x_t \in \mathbb{R}$ describing some amount related to that
object, we might want to \textit{link} then, i.e. bounds on the amount variable
should only be effective iff the decision variable is true. Then, write
\begin{align*}
  x_t \leq M_t \cdot y_t
\end{align*}
where $M_t$ is an upper bound for $x_t$

\paragraph{\iln{Robbers}} About the cars: dont need the information which robber
takes which car, only which car goes where. --- Further, do not need to
constrain car capacity explicity (for which I would have needed the aforemented
variable) but can instead write
\begin{align*}
  \sum_r x_{rb} \leq \sum_{c} z_{cb} \cdot s
\end{align*}



\paragraph{\iln{Examples}}
\begin{itemize}
\item Assignment problem (persons to jobs, assignment costs)
\item 0/1-Knapsack problem (maximise profit while not exceeding budget)
\item set covering (minimise building costs)
\item uncapacitated facility location
\item uncapacitated lot sizing
\end{itemize}


\vfill
\pagebreak
\part{Geometric Algorithms}

The Voronoi set $V(P)$ of a point set $P$ can be interpreted as a planar graph, or as a
subdivison of $\rr^2$.

\paragraph{\ild{Voronoi cell}} \note{Set of points to which $p$ is nearest.}
\begin{align*}
  V(\{p\}) = V(p) = \{ x \in \rr^2 ~|~ \overline{xp} < \overline{xq} \text{~for
  all~} q \in P - \{p\} \}
\end{align*}

\paragraph{\ild{Voronoi edge}} \note{Set of points nearest to at least two
  points in $p$}
\begin{align*}
  V(\{p, p'\}) = \{  x \in \rr^2 ~|~ \overline{xp} = \overline{xp'}
  \land \overline{xp} < \overline{xq} \text{~forall~} q \not = p, p'
  \}
\end{align*}

\paragraph{\thm{Complexity}}
\begin{itemize}
\item The size of \Vor{P} is linear in $n$. It has at most $2n-5$ vertices and
  at most $3n-6$ edges. \proof{Add dummy vertex to make proper planar graph,
    apply eulers formula, handshaking lemma}
\item \Vor{P} can be computed in time $\mathcal{O}(n)$.
\end{itemize}


\paragraph{\thm{Properties}}  
\begin{enumerate}
\item A point $q \in \rr^2$ is a voronoi vertex $\gdw$ $\abs{\partial
    C_P(q) \cap P} \geq 3$ -- \note{i.e. if the largest surrounding empty disc
    has three points at its border}
\item The bisector $b(p_i, p_j)$ \note{(line so that each point of line has
    equal distance to $p_i, p_j$)} defines a voronoi edge $\gdw$ $\exists q \in
  b(p_i, p_j)$ with $\partial C_P(q) \cap P = \{p_i, p_j\}$ -- \note{i.e. can
    find a point on line so that largest surrounding empty disc touches these
    two points.} \label{foo}
\end{enumerate}

\paragraph{\ild{Delaunay Graph}} To a Voronoi partition $\Vor(P)$, we can consider the
dual graph with $P$ as vertex set and $E = \{ (p,q) ~|~ \text{$V(p)$ and $V(q)$
  are adjacent} \}$. Its straight-line drawing (embedding) is the \ild{Delaunay-Graph}.

\paragraph{\thm{Corollaries}} 
\begin{itemize}
\item \DG{P} is planar and crossing-free
\item A voronoi vertex of degree $k$ corresponds to a $k$-gon ($k$-cycle) in
  \DG{P} (\note{$\rightarrow$ triangulation})
\item Points $p,q \in P$ are connected by an edge in \DG{P} $\Leftrightarrow$
  $\exists$ empty circle through $p$ and $q$. \proof{by empty-circle property:
    are connected by edge, thus are adjacent in vor. part., thus there is
    vor.-edge, thus disc is empty by def.}
\item Points $p,q,r$ are vertices of the same face in \DG(P) $\Leftrightarrow$
  circle through $p,q,r$ is empty. \proof{by empty-circle property}.
\item \DG(P) contains an edge connecting the closest pair of points
\item \DG(P) contains an euclidean MST of $P$
\end{itemize}



\section{Sweepline technique}

Think:  • discretize  • identify key events

\subsection{Line segment intersection algorithm}

\paragraph{\textit{Question}} Given a set $S$ of $n$ line segments in $\rr^2$,
do any two segments in $S$ intersect?

\paragraph{\thm{Key observations}}
\begin{itemize}
\item Two segments $s_1, s_2$ intersect iff there exist sweeplines $l_1$, $l_2$
  s.t. $s_1 <_{l_1} s_2$ and $s_1 >_{l_2} s_2$.
\item $l_1$ and $l_2$ can always be chose so that $s_1$ and $s_2$ are direct
  neighbours in the orderings.
\item \note{Thus, only need to consider changes to this ordering}
\end{itemize}

\paragraph{\ila{Data Structure}}
\begin{itemize}
\item \textbf{Sweepline status} for $l$:  all segments intersecting $l$ in order $<_l$
\item \textbf{Event queue} (events relevant for problem): For each of the $2n$
    line segment endpoints, sorted by $x$ coords; case:
    \begin{itemize}
    \item \textit{left endpoint}: insert at correct position in status
    \item \textit{right endpoint}: remove from status
    \item \textit{always}: check for intersection in current status. Given an
      added/removed endpoint, only need to check its neighbours in $<_l$
      (because only this changes). Find these efficiently by using an AVL tree.
    \end{itemize}
  \end{itemize}

\paragraph{\thm{Cor.}} Can check line segment intersection in time
$\mathcal{O}(n \log n)$.


\section{Smallest Enclosing Disc}

\paragraph{\thm{Note}} A disc is uniquely defined by 3 points.

\paragraph{\ila{SED}} in principle...
\begin{enumerate}
\item Start with SED of some two points
\item For each further point, check if it lies in that SED.
\item \textbf{Else}, update SED \note{(the question is how)}
\end{enumerate}

\paragraph{\thm{Lemma}}
\begin{itemize}
\item The SED of $P$ defined by some points $R$ on its boundary is unique.
\item If a new point is within that disc, the SED does not change
\item If a new point is outside that disc, the new SED is defined by $R \cup
  \{p\}$. \iln{i.e. $p$ has to be on the boundary of the new SED}.
\end{itemize}

\paragraph{\ila{SED, SEDWithPoint, SEDWithTwoPoints}} Covering the else case
from above, the point of the recursions is to find ourselves two more points $q$
and $r$ which, together with $p_1$ then uniquely define a disc. Because in
\textit{SEDWIthTwoPoints}, all points $r$ of $P$ are considered, the returned disc
includes ``as many as possible points'' while still running through $p_1$ and
$q$. --- Then bubble back upwards to consider other choices for $q$, $p_1$.

\paragraph{\thm{Backwards Analysis}} What is the probability of making a call to
\textit{SEDWithTwoPoints}? (Look at what happens in \textit{SEWithPoint} and above)
Inside of \textit{SEDWithPoint}, one point $q$ is fixed by the above recursion
layer. --- Think: What is the probability that some iteration $i$ of the for-loop
\textit{was} an else-iteration? After this $i$-th iteration, we have some
intermediary SED $D_i$. The considered point $p_i$ caused the else-branch iff it
is one of the points defining that $D_i$. One of these is fixed ($q$), and up to
now, $i$ points were considered, so we end up with a probability of
$\nicefrac{2}{i}$.

\paragraph{\thm{Cor.}} The SED of $n$ points can be computed in $\mathcal{O}(n)$
\textit{expected} time by a randomized incremental algorithm.

\vfill
\pagebreak
\end{multicols}



\end{document}