\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {part}{I\hspace {1em}Network Flows \& Matchings}{1}
\etoc@startlocaltoc {1}
\contentsline {section}{\numberline {1}Flows}{1}
\contentsline {subsection}{\numberline {1.1}Basics}{1}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {$s-t$-flow}}}{1}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {value}}}{1}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {$s-t$-Cut}}}{1}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Capacity}} of a cut}{1}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Flow value lemma}}}{1}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Weak Duality}}}{1}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Certificate of Optimality}}}{1}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Augmenting Path}}}{1}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Residual Graph}}}{1}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {\textsc {Ford-Fulkerson} Algorithm}}}{1}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Max-flow/Min-cut Theorem}}}{1}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Capacity Scaling}}}{1}
\contentsline {subsection}{\numberline {1.2}Circulations with Demands}{2}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Circulation}}}{2}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Lemma}}}{2}
\contentsline {paragraph}{Max flow formulation}{2}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Thm}}}{2}
\contentsline {subsection}{\numberline {1.3}Circulations with Demands and Lower Bounds}{2}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Circulation with lower Bounds}}}{2}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Thm}}}{2}
\contentsline {subsection}{\numberline {1.4}Integrality}{2}
\contentsline {subsection}{\numberline {1.5}Exercises}{2}
\contentsline {paragraph}{Ex. 2}{2}
\contentsline {paragraph}{Ex. 4}{2}
\contentsline {section}{\numberline {2}Bipartite Matchings}{2}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Max flow formulation}}}{2}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {One-sided canonical decomposition}}}{2}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Thm}}}{2}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {K\IeC {\"o}nig-Egerv\IeC {\'a}ry}}}{2}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Marriage Theorem}}}{2}
\contentsline {subsection}{\numberline {2.1}Edge-Disjoint Paths}{2}
\contentsline {subsection}{\numberline {2.2}Exercises}{3}
\contentsline {part}{II\hspace {1em}Planarity, A* Search, and Randomized Algorithms}{3}
\etoc@startlocaltoc {2}
\contentsline {section}{\numberline {3}Planar Graphs}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Euler's formula}}}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Cor. for planar graphs}}}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Cor. for planar, bipartite graphs}}}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Vert. of deg 5}}}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Kuratowski}}}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Wagner}}}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Cor.}}}{3}
\contentsline {section}{\numberline {4}A*-Search}{3}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Algorithm}}}{3}
\contentsline {paragraph}{Note:}{3}
\contentsline {paragraph}{Termination}{3}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Admissible heuristic}}}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Correctness}}}{3}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Monotonic heuristic}}}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {No re-expansion}}}{3}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Thm}}}{3}
\contentsline {section}{\numberline {5}Randomized Algorithms}{4}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Monte Carlo Algorithm}}}{4}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Las Vegas Algorithm}}}{4}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Miller-Rabin Primality Test}}}{4}
\contentsline {subsection}{\numberline {5.1}Basics of Probability Theory}{4}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Sample Space, Event}}}{4}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Conditional Probability}}}{4}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Independence}}}{4}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Union Bound}}}{4}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Random Variable}}}{4}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Expected Value}} of X}{4}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Linearity of Expectation}}}{4}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Lemma}}}{4}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Waiting Time Bound}}}{4}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Number of correct guesses}}}{4}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Lemma}}}{4}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Coupon Collector's Problem}}}{4}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {QuickSort}}}{4}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Contention Resolution}}}{4}
\contentsline {part}{III\hspace {1em}Structural Decompositions and Algorithms}{5}
\contentsline {section}{\numberline {6}Tree Decompositions}{5}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Tree decomposition}}}{5}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Properties}}}{5}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Variable Elimination for SAT}}}{5}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Small Tree Decomposition}}}{5}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Nice tree decomposition}}}{5}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Thm}}}{5}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Existence of small tree decomp}}}{5}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Lower bound by Minor}}}{5}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Robertson/Seymour}}}{5}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Bags are separators}}}{5}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Notes on DynProg on tree decomps}}}{5}
\contentsline {section}{\numberline {7}Courcelle's Theorem}{5}
\contentsline {paragraph}{\textit {Background}}{5}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Courcelle's Theorem}}}{5}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Note}}}{5}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Note}}}{5}
\contentsline {part}{IV\hspace {1em}Parameterized Algorithms}{6}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Note}}}{6}
\contentsline {section}{\numberline {8}Definitions}{6}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Def}}}{6}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Def}}}{6}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Thm}}}{6}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Note}}}{6}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {A strategy for proving kernels}}}{6}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Kernel for Vertex Cover}}}{6}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Kernel for Hitting Set (Sunflower Lemma)}}}{6}
\contentsline {section}{\numberline {9}Exercises/Examples}{6}
\contentsline {part}{V\hspace {1em}Mathematical Programming}{7}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Linear Programming}}}{7}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {General form}}}{7}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Standard form}}}{7}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Duality gap}}}{7}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Discrete Alternatives or Disjunctions}}}{7}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Linking Variables}}}{7}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Robbers}}}{7}
\contentsline {paragraph}{\leavevmode {\color {OliveGreen}\textit {Examples}}}{7}
\contentsline {part}{VI\hspace {1em}Geometric Algorithms}{8}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Voronoi cell}}}{8}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Voronoi edge}}}{8}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Complexity}}}{8}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Properties}}}{8}
\contentsline {paragraph}{\leavevmode {\color {Purple}\textit {Delaunay Graph}}}{8}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Corollaries}}}{8}
\contentsline {section}{\numberline {10}Sweepline technique}{8}
\contentsline {subsection}{\numberline {10.1}Line segment intersection algorithm}{8}
\contentsline {paragraph}{\textit {Question}}{8}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Key observations}}}{8}
\contentsline {paragraph}{\textit {Data Structure}}{8}
\contentsline {paragraph}{\leavevmode {\color {BrickRed}\textit {Cor.}}}{8}
